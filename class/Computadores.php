<?php

/*
Class para cadastrar computadores
*/

class Computadores{

	private $idInformatica;
	private $enderecoIp;
	private $nomeComputador;
	private $unidadeComputador;
	private $descricaoComputador;

//Get

	public function getIdInformatica(){
		return $this->idInformatica;
	}

	public function getEnderecoIp(){
		return $this->enderecoIp;
	}

	public function getNomeComputador(){
		return $this->nomeComputador;
	}

	public function getUnidadeComputador(){
		return $this->unidadeComputador;
	}

	public function getDescricaoComputador(){
		return $this->descricaoComputador;
	}

//Set
	public function setIdInformatica($idInformatica){
		$this->idInformatica = $idInformatica;
	}

	public function setEnderecoIp($enderecoIp){
		$this->enderecoIp = $enderecoIp;
	}
	public function setNomeComputador($nomeComputador){
		$this->nomeComputador = $nomeComputador
	}
	public function setUnidadeComputador($unidadeComputador){
		$this->unidadeComputador = $unidadeComputador;
	}
	public function setDescricaoComputador($descricaoComputador){
		$this->descricaoComputador = $descricaoComputador;
	}
}

?>